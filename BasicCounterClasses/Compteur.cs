﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounter
{
    public class Compteur
    {
        private static int compteur = 0;

        public static int GetCompteur()
        {
            return compteur;
        }

        public static void SetCompteur(int Value)
        {
            compteur = Value;
        }

        public static void Incrementation()
        {
            compteur++;
        }

        public static void Decrementation()
        {
            compteur--;
        }

        public static void Renit()
        {
            compteur = 0;
        }
    }
}
