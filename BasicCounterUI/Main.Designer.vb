﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FORM_Main
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BT_Diminution = New System.Windows.Forms.Button()
        Me.BT_Augmentation = New System.Windows.Forms.Button()
        Me.BT_Renit = New System.Windows.Forms.Button()
        Me.LBL_TotalText = New System.Windows.Forms.Label()
        Me.LBL_Count = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'BT_Diminution
        '
        Me.BT_Diminution.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BT_Diminution.Location = New System.Drawing.Point(12, 174)
        Me.BT_Diminution.Name = "BT_Diminution"
        Me.BT_Diminution.Size = New System.Drawing.Size(217, 71)
        Me.BT_Diminution.TabIndex = 0
        Me.BT_Diminution.Text = "-"
        Me.BT_Diminution.UseVisualStyleBackColor = True
        '
        'BT_Augmentation
        '
        Me.BT_Augmentation.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BT_Augmentation.Location = New System.Drawing.Point(571, 174)
        Me.BT_Augmentation.Name = "BT_Augmentation"
        Me.BT_Augmentation.Size = New System.Drawing.Size(217, 71)
        Me.BT_Augmentation.TabIndex = 1
        Me.BT_Augmentation.Text = "+"
        Me.BT_Augmentation.UseVisualStyleBackColor = True
        '
        'BT_Renit
        '
        Me.BT_Renit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BT_Renit.Location = New System.Drawing.Point(289, 296)
        Me.BT_Renit.Name = "BT_Renit"
        Me.BT_Renit.Size = New System.Drawing.Size(225, 63)
        Me.BT_Renit.TabIndex = 2
        Me.BT_Renit.Text = "RAZ"
        Me.BT_Renit.UseVisualStyleBackColor = True
        '
        'LBL_TotalText
        '
        Me.LBL_TotalText.AutoSize = True
        Me.LBL_TotalText.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_TotalText.Location = New System.Drawing.Point(369, 74)
        Me.LBL_TotalText.Name = "LBL_TotalText"
        Me.LBL_TotalText.Size = New System.Drawing.Size(80, 33)
        Me.LBL_TotalText.TabIndex = 3
        Me.LBL_TotalText.Text = "Total"
        Me.LBL_TotalText.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LBL_Count
        '
        Me.LBL_Count.AutoSize = True
        Me.LBL_Count.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_Count.Location = New System.Drawing.Point(342, 154)
        Me.LBL_Count.Name = "LBL_Count"
        Me.LBL_Count.Size = New System.Drawing.Size(153, 108)
        Me.LBL_Count.TabIndex = 4
        Me.LBL_Count.Text = "00"
        Me.LBL_Count.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'FORM_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.LBL_Count)
        Me.Controls.Add(Me.LBL_TotalText)
        Me.Controls.Add(Me.BT_Renit)
        Me.Controls.Add(Me.BT_Augmentation)
        Me.Controls.Add(Me.BT_Diminution)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "FORM_Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BT_Diminution As Button
    Friend WithEvents BT_Augmentation As Button
    Friend WithEvents BT_Renit As Button
    Friend WithEvents LBL_TotalText As Label
    Friend WithEvents LBL_Count As Label
End Class
