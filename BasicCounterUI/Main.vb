﻿Public Class FORM_Main

    Private Sub BT_Augmentation_Click(sender As Object, e As EventArgs) Handles BT_Augmentation.Click
        BasicCounter.Compteur.Incrementation()
        LBL_Count.Text = BasicCounter.Compteur.GetCompteur()
    End Sub

    Private Sub BT_Diminution_Click(sender As Object, e As EventArgs) Handles BT_Diminution.Click
        BasicCounter.Compteur.Decrementation()
        LBL_Count.Text = BasicCounter.Compteur.GetCompteur()
    End Sub

    Private Sub BT_Renit_Click(sender As Object, e As EventArgs) Handles BT_Renit.Click
        BasicCounter.Compteur.Renit()
        LBL_Count.Text = BasicCounter.Compteur.GetCompteur()
    End Sub

    Private Sub FORM_Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LBL_Count.Text = BasicCounter.Compteur.GetCompteur()
    End Sub
End Class
