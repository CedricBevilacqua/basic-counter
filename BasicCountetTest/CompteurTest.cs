﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BasicCountetTest
{
    [TestClass]
    public class CompteurTest
    {
        [TestMethod]
        public void GetCompteur()
        {
            Assert.AreEqual(0, BasicCounter.Compteur.GetCompteur());
        }

        [TestMethod]
        public void SetCompteur()
        {
            BasicCounter.Compteur.SetCompteur(10);
            Assert.AreEqual(10, BasicCounter.Compteur.GetCompteur());
        }

        [TestMethod]
        public void Incrementation()
        {
            BasicCounter.Compteur.SetCompteur(0);
            Assert.AreEqual(0, BasicCounter.Compteur.GetCompteur());
            BasicCounter.Compteur.Incrementation();
            Assert.AreEqual(1, BasicCounter.Compteur.GetCompteur());

            BasicCounter.Compteur.SetCompteur(-1);
            BasicCounter.Compteur.Incrementation();
            BasicCounter.Compteur.Incrementation();
            Assert.AreEqual(1, BasicCounter.Compteur.GetCompteur());
        }

        [TestMethod]
        public void Decrementation()
        {
            BasicCounter.Compteur.SetCompteur(5);
            Assert.AreEqual(5, BasicCounter.Compteur.GetCompteur());
            BasicCounter.Compteur.Decrementation();
            Assert.AreEqual(4, BasicCounter.Compteur.GetCompteur());

            BasicCounter.Compteur.SetCompteur(-1);
            BasicCounter.Compteur.Decrementation();
            Assert.AreEqual(-2, BasicCounter.Compteur.GetCompteur());
        }

        [TestMethod]
        public void Renit()
        {
            BasicCounter.Compteur.SetCompteur(5);
            BasicCounter.Compteur.Renit();
            Assert.AreEqual(0, BasicCounter.Compteur.GetCompteur());
        }
    }
}
